module ApplicationHelper

	def categories_unordered_list
		content_tag :ul do
			Category.order('title desc').collect { |c| concat(content_tag(:li, link_to(c, c))) }
		end
	end

end
